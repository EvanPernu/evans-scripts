#!/bin/bash

docker kill $(docker ps -q)
yes | docker container prune
yes | docker image prune
yes | docker volume prune
yes | docker network prune
docker rmi -f $(docker images -q)
echo "gottem coach"