#!/bin/bash

while getopts ":cifs" OPTION
do
    case $OPTION in
        c)
            docker rm $(docker ps -a -q) 2>&1 | tee -a evan.out
            ;;
        i)
            docker rmi $(docker images -q) 2>&1 | tee -a evan.out
            ;;
        f)
            docker rmi -f $(docker images -q) 2>&1 | tee -a evan.out
            ;;
        s)
            docker stop $(docker ps -aq) 2>&1 | tee -a evan.out
            ;;
        \?)
            echo "-c remove all containers"
            echo "-i remove all images"
            echo "-f force remove all images"
            echo "-s stop all containers"
            ;;
    esac
done
